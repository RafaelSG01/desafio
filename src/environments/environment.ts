// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCPvsU9EL86IEsG5KBUlamrEnui7R0CsD0",
    authDomain: "desafioprolins-934d7.firebaseapp.com",
    databaseURL: "https://desafioprolins-934d7.firebaseio.com",
    projectId: "desafioprolins-934d7",
    storageBucket: "desafioprolins-934d7.appspot.com",
    messagingSenderId: "410544240143",
    appId: "1:410544240143:web:d8ec2f8817a71776d2157a",
    measurementId: "G-EJ00VJW1XJ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
