import { Component } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { AngularFireDatabase } from '@angular/fire/database';
import { Geolocation } from '@ionic-native/geolocation';

declare let google;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  private loading: any;
  public fGroup: FormGroup;
  public map: any;
  

  

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl : ToastController,
    private formBuilder: FormBuilder,
    public emailComposer: EmailComposer,
    public db: AngularFireDatabase,
    private geolocation: Geolocation
  ) {
    this.fGroup = this.formBuilder.group({
      'name': [null, Validators.required],
      'email': [null, Validators.compose([
        Validators.required,
        Validators.pattern(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4})$/)
      ])],
      'phone': [null, Validators.compose([
        Validators.required,
        Validators.pattern(/(\(\d{2}\)\s)?(\d{4,5}\-\d{4})/g)
      ])],
      'address': [null, Validators.required]
    });

   }
  async location(){
    this.geolocation.getCurrentPosition().then((resp) =>{
      const position = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      console.log(position);
    }).catch((error)=>{
      console.log('Erro ao localizar');
    });
  }
  
   

  async register(){
    await this.presentLoading();
    
    this.db.database.ref('/forms').push(this.fGroup.value).then(()=>{
      console.log('salvou!');
    });
    
    
      let email = ({
        to: 'rafaelsoares01@gmail.com',
        cc:[],
        bcc:[],
        subject: 'Registro de cadastro do ' + this.fGroup.value.name,
        body: 'Nome: ' + this.fGroup.value.name + '<br>Telefone: ' + this.fGroup.value.phone + '<br>E-mail: ' + this.fGroup.value.email + '<br>Endereço: ' + this.fGroup.value.address,
        isHtml: true,
        app: "gmail"
      });
      this.emailComposer.open(email);
    try{
      
      
    } catch (error){
      console.error(error)
    } finally {
      this.loading.dismiss();
    }
  }
  
  async presentLoading() {
    this.loading = await this.loadingCtrl.create({
      message: 'Enviando registro!'
    });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 4000 });
    toast.present();
  }

}
